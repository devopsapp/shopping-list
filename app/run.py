import logging
import sys

from tg_bot import start_bot
def main():
    start_bot()

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    main()