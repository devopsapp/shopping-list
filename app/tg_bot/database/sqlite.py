import mysql.connector
from tg_bot.config import DB_NAME, DB_PASSWORD, DB_USER, DB_HOST

async def db_start():
    global db, cursor

    db = mysql.connector.connect(user=DB_USER, password=DB_PASSWORD,
                              host=DB_HOST,
                              database=DB_NAME)
    cursor = db.cursor()

    print('Database connected')
    
    cursor.execute('CREATE TABLE IF NOT EXISTS profile(user_id VARCHAR(255), name TEXT, product TEXT)')
    cursor.execute('CREATE TABLE IF NOT EXISTS previous_message(user_id VARCHAR(255) PRIMARY KEY, message_id INTEGER, date TEXT)')
    db.commit()

async def set_previous_message(user_id, message_id, date):
    if await get_previous_message(user_id):
        cursor.execute('UPDATE previous_message SET message_id = %s, date = %s WHERE user_id = %s', (message_id, date, user_id))
    else:
        cursor.execute('INSERT INTO previous_message VALUES(%s,%s,%s)', (user_id, message_id, date))
    db.commit()

async def get_previous_message(user_id):
    cursor.execute('SELECT message_id, date FROM previous_message WHERE user_id =%s', (user_id,))
    message_id = cursor.fetchone()
    return message_id

async def remove_last_message(user_id):
    cursor.execute('DELETE FROM previous_message WHERE user_id =%s', (user_id,))
    db.commit()


async def create_product(user_id, product):
    cursor.execute('INSERT INTO profile VALUES(%s, %s, %s)', (user_id, 'default', product))
    db.commit()

async def remove_all_products(user_id):
    cursor.execute('DELETE FROM profile WHERE user_id =%s', (user_id,))
    db.commit()

async def remove_product(user_id, product):
    cursor.execute('DELETE FROM profile WHERE user_id =%s AND product =%s', (user_id, product))
    db.commit()


async def get_all_products(user_id):
    cursor.execute('SELECT product FROM profile WHERE user_id = %s', (user_id,))
    products = cursor.fetchall()
    return products