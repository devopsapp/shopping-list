from tg_bot.database.sqlite import get_all_products, set_previous_message, get_previous_message, remove_last_message
from tg_bot.keyboard.keyboard import get_all_products_ikb
from aiogram.types import Message
from tg_bot import main

async def get_all_products_mess(message: Message):
    message_answer = None
    try:
        data = await get_all_products(message.chat.id)
        data_list = [item for sublist in data for item in sublist]

        builder = get_all_products_ikb(data_list)

        if len(data_list) == 0:
            await message.answer("Список покупок пуст")
            await remove_last_message(message.chat.id)
        else:
            message_answer = await message.answer(
                                    "Список покупок",
                                    reply_markup=builder.as_markup()
                            )
            await remove_previous_message(message_answer)
    except TypeError:
        message.answer("TypeError")

async def remove_previous_message(message: Message):
    previous_message = await get_previous_message(message.chat.id)
    if (previous_message != None):
        await main.bot.delete_message(message.chat.id, previous_message[0])
    await set_previous_message(message.chat.id, message.message_id, message.date)