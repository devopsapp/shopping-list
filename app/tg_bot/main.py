from os import getenv, path
from dotenv import load_dotenv
from tg_bot.config import TOKEN, BASE_WEBHOOK_URL, WEB_SERVER_HOST, WEB_SERVER_PORT, WEBHOOK_PATH
from tg_bot.database.sqlite import db_start
from tg_bot.handlers.main_router import router

from aiohttp import web

from aiogram import Bot, Dispatcher
from aiogram.enums import ParseMode
from aiogram.webhook.aiohttp_server import SimpleRequestHandler, setup_application



bot = Bot(token=TOKEN, parse_mode=ParseMode.HTML)

async def on_startup(bot: Bot):
    await bot.set_webhook(url=BASE_WEBHOOK_URL + WEBHOOK_PATH)
    await db_start()

def start_bot():
    dp = Dispatcher()
    dp.include_router(router)
    dp.startup.register(on_startup)


    app = web.Application()
    app.router.add_route('GET', '/check', handle)

    webhook_requests_handler = SimpleRequestHandler(
        dispatcher=dp,
        bot=bot
    )

    webhook_requests_handler.register(app, path=WEBHOOK_PATH)

    setup_application(app, dp, bot=bot, on_startup=on_startup)

    web.run_app(app, host=WEB_SERVER_HOST, port=WEB_SERVER_PORT)



async def handle(request):
    html_page = """
    <html>
    <body>
        <h1>200 OK</h1>
    </body>
    </html>
    """
    return web.Response(text=html_page, content_type='text/html', status=200)