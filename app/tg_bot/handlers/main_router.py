from aiogram import F, Router
from aiogram.types import Message, CallbackQuery
from aiogram.filters import CommandStart, Command
from tg_bot.utils.products import get_all_products_mess

from tg_bot.database.sqlite import create_product, remove_all_products, remove_product

router = Router()

@router.message(CommandStart())
async def commnnd_start_handler(message: Message):
    await message.answer("Hello, I'm a shopping list!")

@router.message(Command(commands='help'))
async def process_help_command(message: Message):
    await message.answer(text='/help')

@router.message(Command("clear"))
async def remove_all_products_handlers(message: Message):
    data = await remove_all_products(message.chat.id)
    await message.answer("Список покупок очищен")

@router.message(Command("list"))
async def get_all_products_handlers(message: Message):
    await get_all_products_mess(message)

@router.callback_query(F.data.startswith("remove_"))
async def remove_product_handler(callback: CallbackQuery):
    answer = callback.data.split("_")[1]
    await remove_product(callback.message.chat.id, answer)
    await get_all_products_mess(callback.message)
    await callback.answer()


@router.message()
async def echo_handler(message: Message):
    try:
        await create_product(message.chat.id, message.text)
        await get_all_products_mess(message)
    except TypeError:
        message.answer("TypeError")


