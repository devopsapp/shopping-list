from os import getenv, path
from dotenv import load_dotenv


current_dir = path.abspath(path.join(__file__, '../..'))
load_dotenv(dotenv_path=f'{current_dir}/.env', verbose=True)

TOKEN = f'{getenv("BOT_TOKEN")}'
WEB_SERVER_HOST = getenv("WEB_SERVER_HOST")
WEB_SERVER_PORT = int(getenv("WEB_SERVER_PORT"))
WEBHOOK_PATH = getenv("WEBHOOK_PATH")
BASE_WEBHOOK_URL = getenv("BASE_WEBHOOK_URL")

DB_NAME = getenv("DB_NAME")
DB_PASSWORD = getenv("DB_PASSWORD")
DB_USER = getenv("DB_USER")
DB_HOST = getenv("DB_HOST")