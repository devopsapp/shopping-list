from aiogram.utils.keyboard import InlineKeyboardButton, InlineKeyboardMarkup, InlineKeyboardBuilder
from aiogram import types


def get_all_products_ikb(data) -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()
    for product in data:
        builder.add(types.InlineKeyboardButton(
            text=f'{product}',
            callback_data=f'remove_{product}',
            )
        )
    builder.adjust(1)
    # 
    #     keyboard.add(InlineKeyboardButton(product, callback_data=product))
    return builder